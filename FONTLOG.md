FONTLOG for PicNic
-------------------

This file provides detailed information on the Demo font software.
This information should be distributed along with the Demo fonts
and any derivative works.


Basic Font Information
--------------------------

PicNic is an organic typeface inhabiting a cloud of contextual ligatures and inclusive ligatures, substituting for the midpoint automatically. The inclusive ligatures are designed mainly for French. This ligature work is based on research by the ByeByeBinary collective. https://genderfluid.space/
The shapes are inspired by the trajectory of a drop of water on an oilcloth, the shadows of the tree leaves on the fabric lying in the grass, the precise path that the ant takes to the watermelon. They attempt to translate the sensory experience of a picnic among friends.
This typeface will suit for all wild, poetic and bucolic explorers for their most adventurous excursions. 



Information for Contributors
------------------------------

PicNic is released under the CUTE V.01 - https://genderfluid.space/documents/2024_BBB_CUTE-EN.pdf

For information on what you're allowed to change or modify, consult the CUTE.txt. The Conditions d'utilisations typographiques engageantes (CUTE), written by the Bye Bye Binary collective, are a way of bringing together people who design, distribute and use post-binary fonts. These conditions are a kind of contract, a user guide, usable by anyone wishing to publish a post-binary font. Like a license, like a mussel on its rock, this text travels and spreads with the font files as they are downloaded. They stand out from most free licenses by integrating the question of economics and the material conditions of existence of designers. The critical and political ideas that drive them thus pollinate graphic practices committed to a radically feminist, anti-racist, anti-capitalist, queer and trans* perspective 

See the project website for the current version of the text:

https://genderfluid.space/


ChangeLog
----------

When you make modifications, be sure to add a description of your changes,
following the format of the other entries, to the start of this section.

27 Jan 2022 (Mariel Nils) PicNic v2.0
- Initial release of font "PicNic"


Acknowledgements
-------------------------

When you make modifications, be sure to add your name (N), email (E),
web-address (W) and description (D). This list is sorted by last name in
alphabetical order.

N: Mariel Nils
E: http://nilsmarielle@gmail.com
W: 
D: Author - Latin Glyphs
